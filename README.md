To upgrade to latest angular

`ng update @angular/core @angular/cli ngx-cookie-service jest ts-jest jest-preset-angular @types/jest`

Update the requirements and version here:
`angular-client/projects/joelryan2k/django-file-uploader/package.json`

Run the tests like this:

```
cd angular-client
npm run test
```