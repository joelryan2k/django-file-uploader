const express = require('express')
const path = require('path')
const app = express()
const port = 9000

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'));
})

app.post('/', (req, res) => {
    res.send('ok');
})

app.use('/static', express.static('../angular-client/dist/public-components/'))

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
