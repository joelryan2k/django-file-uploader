from setuptools import setup

setup(name="django_uploads",
    version='1.0.0',
    description='Upload server for django',
    author='Joel Ryan',
    author_email='joel@iwcenter.com',
    packages=['uploads', 'uploads.migrations'],
    install_requires=[
        # "django>=2.2",
        # "django-axes==5.4.3",
        "django-rest-framework"
    ],
    python_requires=">=3.5",
)
