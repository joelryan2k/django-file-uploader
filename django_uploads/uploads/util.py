import tempfile
import uuid

from .models import Upload, Chunk

class UploadReader:
    def __init__(self, upload: Upload, delete_after=True):
        self.upload = upload
        self.delete_after = delete_after

    def __enter__(self):
        self.buffer = tempfile.TemporaryFile()

        for chunk in self.upload.read():
            self.buffer.write(chunk)

        self.buffer.seek(0)

        return self.buffer

    def __exit__(self, type, value, traceback):
        self.buffer.close()

        if self.delete_after:
            self.upload.delete()

def create_upload(source: bytes, **kwargs) -> Upload:
    upload = Upload.objects.create(token=uuid.uuid4(), expected_bytes=len(source), total_bytes=len(source), completed=True, **kwargs)
    Chunk.objects.create(upload=upload, offset=0, length=len(source), data=source)
    return upload
