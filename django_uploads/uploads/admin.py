from django.contrib import admin
from uploads import models

class UploadAdmin(admin.ModelAdmin):
	list_display = [
		'file_name',
		'content_type',
		'created',
		'total_bytes',
		'completed',
	]
	ordering = ('file_name',)
	date_hierarchy = 'created'

admin.site.register(models.Upload, UploadAdmin)