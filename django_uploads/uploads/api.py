import uuid

from django.urls import path, include
from django.http import StreamingHttpResponse

from rest_framework import serializers, viewsets, mixins, routers, parsers, exceptions
from rest_framework.response import Response
from rest_framework.decorators import action

from .models import Upload, Chunk

class UploadCreateSerializer(serializers.Serializer):
    file_name = serializers.CharField()
    content_type = serializers.CharField()
    expected_bytes = serializers.IntegerField()

    class Meta:
        model = Upload
        fields = ['file_name', 'content_type', 'expected_bytes']

class UploadViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = Upload.objects.none()

    def create(self, request, *args, **kwargs):
        serializer = UploadCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        upload = Upload.objects.create(
            file_name=serializer.validated_data['file_name'],
            content_type=serializer.validated_data['content_type'],
            expected_bytes=serializer.validated_data['expected_bytes'],
            token=uuid.uuid4()
        )

        return Response({
            'token': upload.token,
        }, status=201)

    def retrieve(self, request, pk, *args, **kwargs):
        upload = Upload.objects.get(token=pk)

        return StreamingHttpResponse(self.stream_file(pk))

    def stream_file(self, token):
        upload = Upload.objects.get(token=token)
        return upload.read()

    @action(detail=True, methods=['post'], parser_classes=[parsers.MultiPartParser])
    def chunk(self, request, pk, *args, **kwargs):
        data = request.FILES['data'].read()
        upload = Upload.objects.get(token=pk)
        offset = request.query_params.get('offset')
        length = len(data)

        Chunk.objects.create(
            upload=upload,
            offset=offset,
            length=length,
            data=data,
        )

        return Response()

    @action(detail=True, methods=['post'])
    def finalize(self, request, pk, *args, **kwargs):
        total_bytes = 0

        for chunk in Chunk.objects.filter(upload__token=pk).order_by('offset').only('offset', 'length'):
            if chunk.offset != total_bytes:
                return Response('Offset {0} failed'.format(chunk.offset), status=400)
            
            total_bytes += chunk.length

        upload = Upload.objects.get(token=pk)

        if upload.expected_bytes != total_bytes:
            return Response('Size mismatch', status=400)

        Upload.objects.filter(token=pk).update(completed=True, total_bytes=total_bytes)

        return Response()

router = routers.DefaultRouter()
router.register(r'uploads', UploadViewSet, basename='uploads')

urlpatterns = [
    path('/', include(router.urls)),
]
