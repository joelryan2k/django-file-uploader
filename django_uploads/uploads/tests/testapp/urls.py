from django.urls import include, path
from django.conf import settings

urlpatterns = [
    path('api', include(('uploads.urls', 'uploads'), namespace='uploads')),
]
