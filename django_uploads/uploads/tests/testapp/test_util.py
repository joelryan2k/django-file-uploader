from django.test import TestCase

from uploads.util import create_upload, UploadReader

class UtilTests(TestCase):
    def test_create(self):
        source = b'123'
        upload = create_upload(source)

        with UploadReader(upload) as buffer:
            data = buffer.read()
            self.assertEqual(source, data)
