from django.apps import AppConfig


class TestAppConfig(AppConfig):
    name = 'uploads.tests.testapp'
    verbose_name = 'TestApp'
