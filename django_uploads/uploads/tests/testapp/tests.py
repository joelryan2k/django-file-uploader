import io
import datetime

from django.urls import reverse
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils import timezone

from rest_framework.test import APITestCase

from uploads.test_fixtures import UploadFactory, ChunkFactory
from uploads.models import Upload, Chunk

class UploadTests(APITestCase):
    def test_can_start_upload(self):
        url = reverse('uploads:uploads-list')

        response = self.client.post(url, {
            'file_name': 'test.txt',
            'content_type': 'application/foo',
            'expected_bytes': 123,
        }, format='json')

        self.assertEqual(201, response.status_code, response.content)
        upload = Upload.objects.get(token=response.json()['token'])

    def test_can_add_chunk(self):
        upload = UploadFactory()

        url = reverse('uploads:uploads-chunk', args=[upload.token]) + '?offset=0'

        binary_data = '123'.encode('utf-8')
        data = io.BytesIO(binary_data)

        response = self.client.post(url, {
            'data': data,
        }, format="multipart")

        self.assertEqual(200, response.status_code, response.content)
        chunk = Chunk.objects.all()[0]
        self.assertEqual(chunk.data, binary_data)
        self.assertEqual(0, chunk.offset)
        self.assertEqual(3, chunk.length)

    def test_can_finalize(self):
        upload = UploadFactory(
            expected_bytes=15,
        )

        ChunkFactory(
            upload=upload,
            offset=0,
            length=10,
            data=b'1234567890',
        )
        ChunkFactory(
            upload=upload,
            offset=10,
            length=5,
            data=b'abcde',
        )

        url = reverse('uploads:uploads-finalize', args=[upload.token])

        response = self.client.post(url, {}, format="json")

        self.assertEqual(200, response.status_code, response.content)
        upload = Upload.objects.all()[0]
        self.assertEqual(True, upload.completed)
        self.assertEqual(15, upload.total_bytes)

    def test_finalize_fails_if_expected_size_different(self):
        upload = UploadFactory(
            expected_bytes=20
        )

        ChunkFactory(
            upload=upload,
            offset=0,
            length=10,
            data=b'1234567890',
        )
        ChunkFactory(
            upload=upload,
            offset=10,
            length=5,
            data=b'abcde',
        )

        url = reverse('uploads:uploads-finalize', args=[upload.token])

        response = self.client.post(url, {}, format="json")

        self.assertEqual(400, response.status_code, response.content)
        self.assertEqual('Size mismatch', response.json())

    def test_finalize_fails_if_chunk_missing(self):
        upload = UploadFactory(
            expected_bytes=20
        )

        ChunkFactory(
            upload=upload,
            offset=10,
            length=5,
            data=b'abcde',
        )

        url = reverse('uploads:uploads-finalize', args=[upload.token])

        response = self.client.post(url, {}, format="json")

        self.assertEqual(400, response.status_code, response.content)
        self.assertEqual('Offset failed', response.json())


    def test_can_read_uploaded_file(self):
        upload = UploadFactory(completed=True)
        ChunkFactory(
            upload=upload,
            offset=0,
            length=10,
            data=b'1234567890',
        )
        ChunkFactory(
            upload=upload,
            offset=10,
            length=5,
            data=b'abcde',
        )

        url = reverse('uploads:uploads-detail', args=[upload.token])

        response = self.client.get(url)

        self.assertEqual(200, response.status_code)
        self.assertEqual(b'1234567890abcde', b''.join(response.streaming_content))

    def test_purge(self):
        old_upload = UploadFactory(created=timezone.now() - datetime.timedelta(days=31))
        new_upload = UploadFactory()

        Upload.objects.purge()

        uploads = list(Upload.objects.all())
        self.assertEqual(1, len(uploads))
        self.assertEqual(new_upload.id, uploads[0].id)
