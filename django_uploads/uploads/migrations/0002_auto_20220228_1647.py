# Generated by Django 3.0.14 on 2022-02-28 16:47

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('uploads', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='upload',
            name='created',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
