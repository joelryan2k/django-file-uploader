import datetime

from django.db import models
from django.utils import timezone


class UploadManager(models.Manager):
    def purge(self, days=30):
        qs = self.filter(created__lt=timezone.now() - datetime.timedelta(days=days))
        Chunk.objects.filter(upload__in=qs).only('id').delete()
        qs.only('id').delete()

class Upload(models.Model):
    token = models.CharField(max_length=255, unique=True)
    file_name = models.CharField(max_length=1024)
    content_type = models.CharField(max_length=1024)
    created = models.DateTimeField(default=timezone.now)
    expected_bytes = models.IntegerField()
    total_bytes = models.IntegerField(blank=True, null=True)
    completed = models.BooleanField(default=False)

    objects = UploadManager()

    def read(self):
        if not self.completed:
            raise Exception('Upload not complete')

        for chunk in Chunk.objects.filter(upload__token=self.token).order_by('offset'):
            yield chunk.data

class Chunk(models.Model):
    upload = models.ForeignKey(Upload, on_delete=models.CASCADE)
    offset = models.IntegerField()
    length = models.IntegerField()
    data = models.BinaryField()
