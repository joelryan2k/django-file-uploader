import uuid

import factory

from . import models

class UploadFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Upload

    token = factory.Sequence(lambda n: n)
    file_name = 'some_file.txt'
    content_type = 'application/text'
    expected_bytes = 0

class ChunkFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Chunk

