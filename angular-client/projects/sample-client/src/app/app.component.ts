import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  template: `
    <div [formGroup]="form">
      app works!

      <div class="wrapper">
        <uploads-file-multi-input formControlName="uploadedFiles" [multiple]="true"></uploads-file-multi-input>
      </div>

      {{ form.value | json }}
</div>
  `,
  styles: [`
  .wrapper {
    background-color: #eeeeee;
    margin: 50px;
  }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  form: UntypedFormGroup;

  constructor(
    private fb: UntypedFormBuilder
  ) {
    this.form = this.fb.group({
      uploadedFiles: null,
    });
  }

  ngOnInit(): void {
  }

}
