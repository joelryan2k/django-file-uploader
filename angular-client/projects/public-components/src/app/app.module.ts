import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import {
  DjangoFileUploaderModule,
  FileUploadInputComponent,
} from 'projects/joelryan2k/django-file-uploader/src/public-api';

@NgModule({
    declarations: [],
    imports: [
        BrowserModule,
        DjangoFileUploaderModule
    ],
    providers: [],
    exports: []
})
export class PublicComponentsModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const el = createCustomElement(FileUploadInputComponent, {
      injector: this.injector,
    });
    customElements.define('uploads-file-input', el);
  }
}
