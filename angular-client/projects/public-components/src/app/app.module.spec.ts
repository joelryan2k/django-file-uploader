import { Injector } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';

import { PublicComponentsModule } from './app.module';

describe('PublicComponentsModule', () => {
  let injector: Injector;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        PublicComponentsModule
      ]
    });

    injector = TestBed.inject(Injector);
  });

  it(`should create a custom element 'uploads-file-input'`, () => {
    expect(document.createElement('uploads-file-input')).toBeDefined();
  });
});