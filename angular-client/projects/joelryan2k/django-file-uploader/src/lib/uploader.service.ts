import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { catchError, map, mergeMap, switchMap, tap, toArray } from 'rxjs/operators';
import { retryWithBackoff } from './operators';
import { CookieService } from 'ngx-cookie-service';

export interface UploadResponse {
  token$: Observable<string>;
  progress$: Observable<number>;
}

@Injectable({
  providedIn: 'root',
})
export class UploaderService {
  constructor(private http: HttpClient, private cookieService: CookieService) {}

  startUpload(
    uploadServer: string,
    file: File,
    includeCsrfToken: boolean
  ): UploadResponse {
    return this.parseFile(uploadServer, file, includeCsrfToken);
  }

  private parseFile(
    uploadServer: string,
    toUpload: File,
    includeCsrfToken: boolean
  ): UploadResponse {
    const uploadConcurrency = 4;

    const headers: any = {};

    if (includeCsrfToken) {
      headers['X-CSRFToken'] = this.cookieService.get('csrftoken');
    }

    const progress$ = new Subject<number>();

    const token$ = of(toUpload).pipe(
      switchMap((f) =>
        this.http.post<{ token: string }>(
          `${uploadServer}/api/uploads/`,
          {
            fileName: f.name,
            contentType: f.type || 'application/octet-stream',
            expectedBytes: f.size,
          },
          {
            headers,
          }
        )
      ),
      map(({ token }) => token),
      mergeMap((token) => {
        const chunks = this.createChunks(toUpload);

        const totalChunkCount = chunks.length;
        let completedChunkCount = 0;

        return of(...chunks).pipe(
          mergeMap(({ offset, length, file }) => {
            const fd = new FormData();
            fd.append('data', file.slice(offset, offset + length));
            return this.http.post(
              `${uploadServer}/api/uploads/${token}/chunk/?offset=${offset}`,
              fd,
              {
                headers,
              }
            ).pipe(
              retryWithBackoff(1000, 50),
              tap(() => {
                completedChunkCount++;
                progress$.next(Math.ceil((completedChunkCount / totalChunkCount) * 100));
              }),
            );
          }, uploadConcurrency),
          toArray(),
          map(() => token)
        );
      }),
      mergeMap((token) =>
        this.http
          .post<{ token: string }>(
            `${uploadServer}/api/uploads/${token}/finalize/`,
            {},
            { headers }
          )
          .pipe(map(() => token))
      )
    );

    return {
      token$,
      progress$,
    };
  }

  private createChunks(
    file: File
  ): { file: File; offset: number; length: number }[] {
    const chunkSize = 512 * 1024;
    const chunks: {
      file: File;
      offset: number;
      length: number;
    }[] = [];

    let offset = 0;

    while (offset < file.size) {
      length = chunkSize;

      if (offset + length > file.size) {
        length = file.size - offset;
      }

      chunks.push({
        file,
        offset,
        length,
      });

      offset += length;
    }

    return chunks;
  }
}
