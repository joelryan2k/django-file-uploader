import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, retryWhen } from 'rxjs/operators';

const getErrorMessages = (maxRetry: number) => `giving up after ${maxRetry} tries`;

export function retryWithBackoff(
  delayMs: number,
  maxRetry = 5,
  backoffMs = 1000
): (src: Observable<any>) => Observable<any> {
  let retries = maxRetry;

  return (src: Observable<any>) =>
    src.pipe(
      retryWhen((errors: Observable<any>) =>
        errors.pipe(
          mergeMap((error) => {
            if (retries-- > 0) {
              console.log('Retrying!');
              const backoffTime = delayMs + (maxRetry - retries) + backoffMs;
              return of(error).pipe(delay(backoffTime));
            }
            return throwError(getErrorMessages(maxRetry));
          })
        )
      )
    );
}
