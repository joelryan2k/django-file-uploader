import { Component, OnInit, forwardRef, ViewChild, ElementRef, Input, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { of, Subject, Subscription } from 'rxjs';
import { catchError, take, tap } from 'rxjs/operators';
import { UploaderService } from './uploader.service';

@Component({
  selector: 'uploads-file-input',
  template: `
    <div class="container">
    <input class="file-input" [hidden]="uploading" accept="/*" type="file" #file (change)="doUpload(file.files)" />

    <div class="uploading-status" *ngIf="progress$ | async as progress">
      Uploading {{ uploadingFilename }} ... <progress id="file" max="100" [value]="progress"> {{ progress }}% </progress>
    </div>

    <button class="clear-button" *ngIf="uploadComplete" type="button" (click)="clear()">X</button>
    <button class="cancel-button" *ngIf="uploading" type="button" (click)="cancel()">Cancel</button>
    </div>
  `,
  styles: [`
    .file-input, .uploading-status, .clear-button, .cancel-button {
      display: inline-block;
    }

    [hidden] { display: none !important;}
  `],
  encapsulation: ViewEncapsulation.ShadowDom,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FileUploadInputComponent),
      multi: true,
    }
  ],
})
export class FileUploadInputComponent implements OnInit, ControlValueAccessor {
  @Input() uploadServer = '';
  @Input() includeCsrfToken = false;
  @Output() newToken = new EventEmitter<string|null>();
  @Output() uploadStart = new EventEmitter<void>();
  @Output() uploadCancel = new EventEmitter<void>();
  @Output() uploadFinish = new EventEmitter<void>();

  disabled = false;
  uploadComplete = false;
  uploadingFilename = '';
  uploading = false;

  @ViewChild('file', { static: true })
  file!: ElementRef;

  progress$ = new Subject<number>();
  uploadSubscription: Subscription|null = null;

  private onChange: any;
  private onTouched: any;

  constructor(private uploaderService: UploaderService) {}

  ngOnInit(): void {
    //
  }

  private _emitChange(value: string|null): void {
    // this is for ngModel
    if (this.onChange) {
      this.onChange(value);
    }

    // for clients not using ngModel
    this.newToken.emit(value);
  }

  private _clear(): void {
    this._emitChange(null);
    this.uploadComplete = false;
    this.uploading = false;
  }

  clear(): void {
    this.file.nativeElement.value = '';
    this._clear();
  }

  cancel(): void {
    this.uploadSubscription?.unsubscribe();
    this.progress$.next(0);
    this.clear();
    this.uploadCancel.emit();
  }

  doUpload(files: FileList|null): void {
    if (!files) {
      return;
    }

    this._clear();

    this.uploading = true;
    this.uploadStart.emit();

    this.uploadingFilename = files[0].name;
    const upload = this.uploaderService.startUpload(this.uploadServer, files[0], this.includeCsrfToken);

    this.uploadSubscription = upload.token$.pipe(
      take(1),
      tap((token) => {
        this.uploadComplete = true;
        this.uploadSubscription = null;
        this.progress$.next(0);
        this.uploading = false;
        this.uploadFinish.emit();
        this._emitChange(token);
      }),
      catchError((error) => {
        console.error(error);
        alert('There was an error uploading the file.');
        return of();
      })
    ).subscribe();

    upload.progress$.subscribe(this.progress$);
  }

  // ControlValueAccessor
  writeValue(): void {
    console.warn('cannot write a value to this input');
  }

  registerOnChange(fn: (rating: number) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
