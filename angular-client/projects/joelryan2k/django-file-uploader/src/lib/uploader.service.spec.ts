import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { UploaderService } from './uploader.service';

describe('UploaderService', () => {
  let service: UploaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(UploaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
