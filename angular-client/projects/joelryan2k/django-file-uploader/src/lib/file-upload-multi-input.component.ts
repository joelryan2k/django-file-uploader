import {
  Component,
  forwardRef,
  ViewChild,
  ElementRef,
  Input,
  ViewEncapsulation,
  ChangeDetectorRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { of, Subscription } from 'rxjs';
import { catchError, take, tap } from 'rxjs/operators';
import { UploaderService, UploadResponse } from './uploader.service';

export interface FileUpload {
  token: string | undefined;
  complete: boolean;
  fileName: string;
  progress: number;
  upload: UploadResponse | undefined;
  subscription: Subscription | undefined;
}

@Component({
  selector: 'uploads-file-multi-input',
  template: `
    <div class="container">
      <input
        class="file-input"
        accept="/*"
        type="file"
        #file
        [multiple]="multiple"
        (change)="doUpload(file.files)"
      />

      <ul>
        <li *ngFor="let upload of uploads">
          {{ upload.fileName }}
          <progress
            *ngIf="!upload.complete"
            max="100"
            [value]="upload.progress"
          >
            {{ upload.progress }}%
          </progress>
          <button (click)="cancel(upload)">X</button>
        </li>
      </ul>
    </div>
  `,
  styles: [
    `
      .file-input,
      .uploading-status,
      .clear-button,
      .cancel-button {
        display: inline-block;
      }

      [hidden] {
        display: none !important;
      }
    `,
  ],
  encapsulation: ViewEncapsulation.ShadowDom,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FileUploadMultiInputComponent),
      multi: true,
    },
  ],
})
export class FileUploadMultiInputComponent implements ControlValueAccessor {
  @Input() uploadServer = '';
  @Input() includeCsrfToken = false;
  @Input() multiple = false;

  @ViewChild('file', { static: true })
  file!: ElementRef;

  uploads: FileUpload[] = [];

  private onChange: any;
  private onTouched: any;

  constructor(
    private uploaderService: UploaderService,
    private cdr: ChangeDetectorRef
  ) {}

  private _emitChange(): void {
    // this is for ngModel
    if (this.onChange) {
      const value = this.uploads
        .filter((upload) => upload.complete)
        .map((upload) => upload.token);
      this.onChange(value);
    }
  }

  cancel(upload: FileUpload): void {
    upload?.subscription?.unsubscribe();
    this.uploads.splice(this.uploads.indexOf(upload), 1);
    this._emitChange();
  }

  doUpload(files: FileList | null): void {
    if (!files) {
      return;
    }

    if (!this.multiple && this.uploads.length > 0) {
      for (const upload of [...this.uploads]) {
        this.cancel(upload);
      }
    }

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < files.length; i++) {
      const file = files[i];

      const upload: FileUpload = {
        fileName: file.name,
        token: undefined,
        complete: false,
        upload: undefined,
        subscription: undefined,
        progress: 0,
      };

      this.uploads.push(upload);

      const uploadResult = this.uploaderService.startUpload(
        this.uploadServer,
        file,
        this.includeCsrfToken
      );

      const subscription = uploadResult.token$
        .pipe(
          take(1),
          tap((result) => {
            upload.complete = true;
            upload.token = result;
            this._emitChange();
            this.cdr.markForCheck();
          }),
          catchError((error) => {
            alert('There was an error uploading the file.');
            console.error(error);
            return of();
          })
        )
        .subscribe();

      uploadResult.progress$.subscribe((progress) => {
        upload.progress = progress;
        this.cdr.markForCheck();
      });

      upload.subscription = subscription;
      upload.upload = uploadResult;
    }

    this.file.nativeElement.value = '';
  }

  // ControlValueAccessor
  writeValue(): void {
    console.warn('cannot write a value to this input');
  }

  registerOnChange(fn: (rating: number) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    // this.disabled = isDisabled;
  }
}
