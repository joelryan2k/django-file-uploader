import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FileUploadInputComponent } from './file-upload-input.component';
import { FileUploadMultiInputComponent } from './file-upload-multi-input.component';



@NgModule({ declarations: [
        FileUploadInputComponent,
        FileUploadMultiInputComponent,
    ],
    exports: [
        FileUploadInputComponent,
        FileUploadMultiInputComponent,
    ], imports: [CommonModule,
        ReactiveFormsModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class DjangoFileUploaderModule { }
